# node-varint

Variable length integer encoding



## Example
```
const varint = require('./');
var toEncode = 1486431798; // unix ts for 2017-02-06 17:43:18
var buf = new Buffer(9);

// returns the buffer length of the encoded result
var len = varint.PutVarint64(buf,toEncode);

var expectedHex = "fb58992636"
if (buf.slice(0,len).toString("hex") == expectedHex) console.log('Hey it worked!');


var buf = new Buffer("fb58992636","hex");
var newbuf = new Buffer(9);

var newlen = varint.GetVarint64(buf,len,newbuf);

if(varint.toNumber(newbuf.slice(0,newlen)) == toEncode) console.log("Hey it works that way too!");

```
